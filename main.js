const get = require("simple-get")

async function register({
  registerHook,
  registerSetting,
  peertubeHelpers,
  settingsManager
}) {
  // see https://docs.hcaptcha.com/api#getapikey
  registerSetting({
    name: 'hcaptcha-site-key',
    label: 'hCaptcha Site Key',
    type: 'input',
    private: false
  })
  registerSetting({
    name: 'hcaptcha-secret-key',
    label: 'hCaptcha Secret Key',
    type: 'input',
    private: true
  })

  registerHook({
    target: "filter:api.user.signup.allowed.result",
    handler: (result, params) => verifyCaptcha(result, params, settingsManager, peertubeHelpers)
  })
}

async function unregister() {
  return
}

module.exports = {
  register,
  unregister
}

async function verifyCaptcha (result, params, settingsManager, peertubeHelpers) {
  if (!params || !params.body) return result;
  
  // g-recaptcha-response is the key that browser will generate upon form submit.
  // if its blank or null means user has not selected the captcha, so return the error.
  if (!params.body["h-captcha-response"]) {
    return { allowed: false, errorMessage: "Captcha wasn't filled" }
  }

  const secretKey = await settingsManager.getSetting('hcaptcha-secret-key')
  if (!secretKey) return result

  return get({
    url: "https://hcaptcha.com/siteverify",
    method: "POST",
    secret: secretKey,
    respons: params.body["h-captcha-response"],
    remoteip: params.ip
  }, function (err, res, body) {
    try {
      body = JSON.parse(body)
    } catch(err) {
      return { allowed: false, errorMessage: "Unparseable reply!" }
    }
    if (body.success !== undefined && !body.success) {
      return { allowed: false, errorMessage: "Wrong captcha" }
    }
    return result
  })
}
